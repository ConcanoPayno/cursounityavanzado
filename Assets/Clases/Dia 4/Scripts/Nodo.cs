﻿using System;

namespace CursoAvanzado.Dia4{
	/// <summary>
	/// La clase Nodo se usará en cada iteración de búsqueda del algoritmo A*.
	/// </summary>
	public class Nodo : IComparable {
		public Punto Posicion; //Posición del nodo
		public bool SePuedeCaminar; //Se puede caminar por este nodo?

		public float DistanciaRecorrida; //Distancia recorrida desde el inicio hasta este nodo
		public float DistanciaRestante; //Distancia en linea recta desde este nodo al destino

		// Distancia total desde el inicio hasta el final pasando por este nodo
		public float DistanciaTotal 
		{ 
			get 
			{ 
				return DistanciaRestante + DistanciaRestante; 
			} 
		}

		public Nodo NodoPredecesor; // Nodo del que viene este nodo
		public EstadoNodo Estado; // Estado del nodo


        public Nodo(Nodo padre, Punto posicion, Punto destino)
        {
            Posicion = posicion;
            NodoPredecesor = padre;
            SePuedeCaminar = true;
            DistanciaRestante = Punto.Distancia(Posicion, destino);
            Estado = EstadoNodo.SinProbar;

            if ( padre != null)
            {
                DistanciaRecorrida = padre.DistanciaRecorrida + Punto.Distancia ( padre.Posicion, Posicion);
            }
            else
            {
                DistanciaRecorrida = 0;
            }
        }

        public override bool Equals ( object other ){

			Nodo otro = (Nodo)other;
			if (ReferenceEquals (null, otro))
				return false;
			return Posicion.Equals ( otro.Posicion );
		}

        public override int GetHashCode()
        {
            return Posicion.GetHashCode();
        }

        public override string ToString()
        {
            return "Nodo: ["+Posicion.x+","+Posicion.y+"] -> "+(SePuedeCaminar?"":"No ")+"Caminable , "+Estado;
        }

        public int CompareTo(object other)
        {
            Nodo otro = (Nodo)other;
            if (ReferenceEquals(null, otro))
                return int.MaxValue;
            return (int)((DistanciaTotal - otro.DistanciaTotal)*1000);
        }
    }

	public enum EstadoNodo { 
		SinProbar, // Nodo sin probar aún
		Abierto, // Nodo descubierto
		Cerrado } // Nodo cerrado porque hay nodos mejores


}
