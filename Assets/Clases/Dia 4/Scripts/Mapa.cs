﻿#define NO_PROBANDO

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CursoAvanzado.Dia4{
	public class Mapa : MonoBehaviour {

        public Transform suelo;

		private bool[,] mapa = { { true } };

		public int ancho, alto;


        private int celdasCaminables = 0;

        //Con este operador lo que conseguimos es poder acceder al array de booleanos del mapa desde un objeto de tipo Mapa
        public bool this[int i, int j]
        {
            get
            {
                return mapa[i,j];
            }
        }

        public int CeldasCaminables { get { return celdasCaminables; } }

        private void Start()
        {
            if (ancho <= 2) ancho = 3;
            if (alto <= 2) alto = 3;

            if (ancho % 2 == 0) ancho++;
            if (alto % 2 == 0) alto++;


            suelo.position = new Vector2(ancho / 2, alto / 2);
            suelo.localScale = new Vector3(ancho / 10f, 1, alto / 10f);

            mapa = GenerarMapa(ancho, alto);

            foreach ( Transform t in transform)
            {
				if (string.Equals("Cube", t.name) )
                    StartCoroutine(OnValidateDestroy(t.gameObject));

            }

            for (int i = 0; i < ancho; i++)
            {
                for (int j = 0; j < alto; j++)
                {
                    if (!mapa[i, j])
                    {
                        GameObject muro = GameObject.CreatePrimitive(PrimitiveType.Cube);
                        muro.transform.position = new Vector2(i, j);
                        muro.transform.parent = transform;
                    }
                }
            }
        }

        IEnumerator OnValidateDestroy(GameObject go)
        {
            yield return null;
            DestroyImmediate(go);
        }

        public bool[,] GenerarMapa(int ancho, int alto)
        {
            bool[,] mapa = new bool[ancho, alto];
            bool[,] submapa = GenerarSubMapa(ancho - 2, alto - 2);

            for ( int i= 0; i<ancho; i++)
            {
                for ( int j=0; j<alto; j++)
                {
                    if (i == 0 || j == 0 || i == ancho - 1 || j == alto - 1)
                        mapa[i, j] = false; //Es un borde del mapa. No se puede caminar
                    else
                        mapa[i, j] = submapa[i - 1, j - 1];
                    if (mapa[i, j])
                        celdasCaminables++;
                }
            }
            
            return mapa;
        }

        private bool[,] GenerarSubMapa(int ancho, int alto)
        {
#if PROBANDO
            Debug.Log(ancho + ", " + alto);
#endif
            bool[,] submapa = new bool[ancho, alto];

            if (ancho == 1 || alto == 1) {
                for( int i = 0; i< ancho; i++)
                {
                    for ( int j= 0; j<alto; j++)
                    {
                        submapa[i, j] = true;
                    }
                }
                return submapa;
            }
            
            int paredVertical = Random.Range(0,ancho/2) * 2 + 1;
            int paredHorizontal = Random.Range(0, alto/2) * 2 + 1;

            int agujeroX1 = Random.Range(0, (paredVertical+1)/2) * 2;
            int agujeroX2 = Random.Range(0, (ancho-paredVertical)/2) * 2 + 1 + paredVertical;
            int agujeroY = Random.Range(0, alto/2) * 2;




            bool[,] sub1, sub2, sub3, sub4;

            sub1 = GenerarSubMapa( ancho - paredVertical - 1, alto - paredHorizontal - 1);  //Cuadrante 1
            sub2 = GenerarSubMapa( paredVertical , alto - paredHorizontal - 1);          //Cuadrante 2
            sub3 = GenerarSubMapa( paredVertical , paredHorizontal );                 //Cuadrante 3
            sub4 = GenerarSubMapa( ancho - paredVertical - 1, paredHorizontal );         //Cuadrante 4
#if PROBANDO
            string output = "";
#endif
            //Cuadrante 3
            for ( int i = 0; i<paredVertical; i++)
            {
                for (int j = 0; j < paredHorizontal ; j++)
                {
                    if (sub3[i, j])
                    {
                        submapa[i, j] = true;
#if PROBANDO
                        output += "[" + i + "," + j + "] = " + submapa[i, j];
#endif
                    }
                }
            }
            //Cuadrante 1
            for (int i = paredVertical+1; i < ancho; i++)
            {
                for (int j = paredHorizontal+1; j < alto; j++)
                {
                    //submapa[i, j] = sub1[i-paredVertical-1, j-paredHorizontal-1];
                    if (sub1[i - paredVertical - 1, j - paredHorizontal - 1])
                    {
                        submapa[i, j] = true;
#if PROBANDO
                        output += "[" + i + "," + j + "] = " + submapa[i, j];
#endif
                    }
                }
            }
            //Cuadrante 2
            for (int i = 0; i < paredVertical; i++)
            {
                for (int j = paredHorizontal + 1; j < alto; j++)
                {
                    //submapa[i, j] = sub2[i , j - paredHorizontal-1];
                    if (sub2[i, j - paredHorizontal - 1])
                    {
                        submapa[i, j] = true;
#if PROBANDO
                        output += "[" + i + "," + j + "] = " + submapa[i, j];
#endif
                    }
                }
            }
            //Cuadrante 4
            for (int i = paredVertical + 1; i < ancho; i++)
            {
                for (int j = 0; j < paredHorizontal; j++)
                {
                    //submapa[i, j] = sub4[i - paredVertical-1, j];
                    if (sub4[i - paredVertical - 1, j])
                    {
                        submapa[i, j] = true;
#if PROBANDO
                        output += "[" + i + "," + j + "] = " + submapa[i, j];
#endif
                    }
                }
            }

#if PROBANDO
            output += " OOOOOO-> [" + agujeroX1 + "," + paredHorizontal + "] = PUERTA";
            output += "[" + agujeroX2 + "," + paredHorizontal + "] = PUERTA";
            output += "[" + paredVertical + "," + agujeroY + "] = PUERTA";

            Debug.Log("Output: " + output);
#endif

            submapa[agujeroX1 , paredHorizontal] = true;
            submapa[agujeroX2 , paredHorizontal] = true;
            submapa[paredVertical , agujeroY] = true;

            return submapa;
        }

        /// <summary>
        /// Distancia en línea recta entre un punto <param name="a"></param> y un punto <param name="b"></param>
        /// </summary>
		public float Distancia (Punto a, Punto b)
        {
            return Punto.Distancia(a, b);
		}
        

		public List<Punto> Vecinos ( Punto punto , bool incluirDiagonales = false )
		{
			List<Punto> vecinos = new List<Punto> ();

            int xMin = Mathf.Max(1, punto.x - 1);
            int xMax = Mathf.Min(ancho-2, punto.x + 1);
            int yMin = Mathf.Max(1, punto.y - 1);
            int yMax = Mathf.Min(alto-2, punto.y + 1);
            
			for (int i = xMin; i <= xMax; i++) {
				for (int j = yMin; j <= yMax; j++) {
					//El propio punto tampoco interesa añadirlo
					if (i == punto.x && j == punto.y)
						continue;
                    //Si no se mueve en diagonal no incluimos los puntos diagonales
                    if (!incluirDiagonales && i!=punto.x && j!=punto.y )
                        continue;
                    //Si es un punto caminable, lo añadimos. No añadimos paredes
					try{
                    if ( mapa[i,j] )
					    vecinos.Add (new Punto(i, j));
					}catch ( System.Exception e ){
						Debug.LogError (" X: " + i + " , Y: " + j);
					}
				}
			}
			return vecinos;
		}
	}
}