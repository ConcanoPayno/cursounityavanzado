﻿#define NO_PROBANDO
using System.Collections.Generic;
using UnityEngine;

namespace CursoAvanzado.Dia6{
	public class Buscador : MonoBehaviour {

		private static List<Nodo> nodos;

#if PROBANDO
        private static int limitadorBucleInfinito = 0;
        private static Transform padreDebug;
#endif
        public static List<Punto> Buscar ( Mapa mapa, Punto inicio, Punto destino )
		{
			Nodo inicial = new Nodo(null, inicio, destino);

            nodos = new List<Nodo>() { inicial };
#if PROBANDO
            limitadorBucleInfinito = 0;
            padreDebug = new GameObject("Debug").transform;
            padreDebug.position = Vector3.zero;
#endif

            List<Nodo> camino = BuscarRecursivo (mapa, inicial, destino
#if PROBANDO
                ,Color.red
#endif
                );
            camino.Add(inicial);

			List<Punto> puntos = new List<Punto> ();
            
            for ( int i= camino.Count-1; i>=0; i--)
            {
                puntos.Add(camino[i].Posicion);
            }
            
			return puntos;
		}

        private static List<Nodo> BuscarRecursivo(Mapa mapa, Nodo actual, Punto destino
#if PROBANDO
            , Color color
#endif
            )
        {
#if PROBANDO
            GameObject p = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            p.GetComponent<MeshRenderer>().material.color = color;
            p.transform.position = new Vector3(actual.Posicion.x, actual.Posicion.y, 0);
            p.transform.parent = padreDebug;

            GameObject n = new GameObject("" + limitadorBucleInfinito);
            n.transform.parent = p.transform;
            n.transform.localPosition = Vector3.zero;
            n.transform.localScale = Vector3.one * 0.2f;

            TextMesh mesh = n.AddComponent<TextMesh>();
            mesh.text = n.name;
            mesh.fontSize = 32;
            mesh.alignment = TextAlignment.Center;
            mesh.anchor = TextAnchor.MiddleCenter;

            //p.transform.localScale = Vector3.one * 0.5f;
            limitadorBucleInfinito++;
            if ( limitadorBucleInfinito >= 1000)
            {
                return new List<Nodo>();
			}
			Debug.Log("Siguiente en probar " + actual);
#endif


            actual.Estado = EstadoNodo.Cerrado;

            List<Nodo> vecinos = Vecinos(mapa, actual, destino);

            List<Nodo> camino = new List<Nodo>();

            
            foreach (var vecino in vecinos)
            {

                if (vecino.Posicion.Equals(destino))
                {
                    //Hemos llegado a nuestro destino
                    return new List<Nodo>() { vecino };
                }
                else
                {
                    camino = BuscarRecursivo(mapa, vecino, destino
#if PROBANDO
                            , Color.Lerp(Color.red, Color.green, 2 * limitadorBucleInfinito / (float)mapa.CeldasCaminables )
#endif
                            );
                    //Si el camino contiene el destino, lo devolvemos y no seguimos buscando
                    if ( camino.Count> 0  && camino[0].Posicion.Equals(destino))
                    {
                        camino.Add(vecino);
                        return camino;
                    }
                }
            }
            //No se ha encontrado el destino
            return new List<Nodo>();
        }


        private static List<Nodo> Vecinos(Mapa mapa, Nodo padre, Punto destino)
        {
            List<Punto> vecinos = mapa.Vecinos(padre.Posicion);
            List<Nodo> lista = new List<Nodo>();

            foreach (var v in vecinos)
            {
                if ( v.Equals ( destino ))
                {//Si el vecino v es el destino, no necesitamos devolver el resto de vecinos
                    return new List<Nodo>() { new Nodo(padre, v, destino) };
                }

                Nodo existente = nodos.Find(x => x.Posicion.Equals(v));
                if (existente != null)
                {
                    if (!existente.SePuedeCaminar)
                        continue;
                    if (existente.Estado == EstadoNodo.Cerrado)
                        continue;
                    if (existente.Estado == EstadoNodo.Abierto)
                    {
                        float distanciaRecta = Punto.Distancia(padre.Posicion, destino);
                        float tempRecorrido = padre.DistanciaRecorrida + distanciaRecta;

                        if (tempRecorrido < existente.DistanciaRecorrida)
                        {
                            existente.NodoPredecesor = padre;
                            lista.Add(existente);
                        }
                    }
                    else //Estado SinProbar
                    {
                        existente.NodoPredecesor = padre;
                        existente.Estado = EstadoNodo.Abierto;
                        lista.Add(existente);
                    }
                }
                else
                {
                    Nodo nuevo = new Nodo(padre, v, destino);
                    nodos.Add(nuevo);
                    lista.Add(nuevo);
                }
            }

            lista.Sort();
            

            return lista;
        }

    }
}
