﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


/// <summary>
/// Clase que sirve para enviar un evento pasando un entero por parámetro
/// Necesita el Serializable para poder verlo en el inspector
/// </summary>
/// 
namespace CursoAvanzado.Dia6{
	[System.Serializable]
	public class MyEvent : UnityEvent<Punto>
	{

	}

	public class ControlEntrada : MonoBehaviour {
	    
		public UnityEvent OnMove;
		public MyEvent OnOther;

		public static List<Comando> oldCommands = new List<Comando>();
		public static bool repetir;

		private bool repitiendo;
		private Coroutine replayCoroutine;
		private Vector3 startPoint;

		// Use this for initialization
		void Start () {
			startPoint = transform.position;
			if (OnOther == null)
				OnOther = new MyEvent ();
			OnOther.Invoke (new Punto (1,1));
		}

		void Update (){
			if (!repitiendo)
				RegistroTeclas ();
			if (repetir)
				Repetir ();
		}

		void RegistroTeclas () {
			Punto puntoActual = transform.position;

			if (Input.GetKeyDown (KeyCode.W)) {
				new ComandoArriba (){ objeto = transform }.Ejecutar ();
				OnMove.Invoke ();
			}else if (Input.GetKeyDown (KeyCode.A)) {
				new ComandoIzquierda (){ objeto = transform }.Ejecutar ();
				OnMove.Invoke ();
			}else if (Input.GetKeyDown (KeyCode.S)) {
				new ComandoAbajo (){ objeto = transform }.Ejecutar ();
				OnMove.Invoke ();
			}else if (Input.GetKeyDown (KeyCode.D)) {
				new ComandoDerecha (){ objeto = transform }.Ejecutar ();
				OnMove.Invoke ();
			}else if (Input.GetKeyDown (KeyCode.R)) {
				new ComandoRepeticion (){ objeto = transform }.Ejecutar ();
			}
		}

		void Repetir (){
			if ( oldCommands.Count > 0)
			{
				repetir = false;

				if ( replayCoroutine != null)
				{
					StopCoroutine(replayCoroutine);
				}

				replayCoroutine = StartCoroutine(Repeticion());
			}
		}

		IEnumerator Repeticion (){

			repitiendo = true;

			transform.position = startPoint;

			for (int i = 0; i < oldCommands.Count; i++) {
				oldCommands[i].Hacer();
				yield return new WaitForSeconds (0.5f);
			}

			repitiendo = false;
		}
	}
}