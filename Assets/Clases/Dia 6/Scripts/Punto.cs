﻿using UnityEngine;

namespace CursoAvanzado.Dia6 {
	public struct Punto {
		public int x; //Coordenada X
		public int y; //Coordenada Y

		public Punto ( int _x, int _y ){
			x = _x;
			y = _y;
		}

        public static float Distancia ( Punto a, Punto b)
        {
            float x = Mathf.Abs(a.x - b.x);
            float y = Mathf.Abs(a.y - b.y);

            return Mathf.Sqrt(x * x + y * y);
        }

		public static implicit operator Punto ( Vector3 vector){
			return new Punto (Mathf.FloorToInt(vector.x),Mathf.FloorToInt(vector.y));
		}

		public static implicit operator Punto ( Transform transform){
			return new Punto (Mathf.FloorToInt(transform.position.x),Mathf.FloorToInt(transform.position.y));
		}

		public static implicit operator Punto ( GameObject gameObject){
			return new Punto (Mathf.FloorToInt(gameObject.transform.position.x),Mathf.FloorToInt(gameObject.transform.position.y));
		}

		public static implicit operator Vector3 ( Punto posicion){
			return new Vector3( posicion.x, posicion.y, 0);
		}

		public override bool Equals ( object other ){

			Punto otro = (Punto)other;
			if (ReferenceEquals (null, otro))
				return false;
			return otro.x == x && otro.y == y;
		}

        public override int GetHashCode()
        {
            return (x + 2) ^ (y + 2);
        }


        public override string ToString ()
		{
			return "["+x+","+y+"]";
		}
	}
}
