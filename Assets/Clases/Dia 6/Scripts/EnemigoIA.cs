﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CursoAvanzado.Dia6
{
    public class EnemigoIA : MonoBehaviour
    {
		//
        public enum Estado {Patrullando, Persiguiendo };
        
		private bool vivo;
        private Estado estado;

        //Variables de patrulla
        public int velocidadPatrulla; // 
        public GameObject[] puntosDeSeguimiento;
		private List<Punto>[] rutas;
        private int puntoActual = 0;
		private int posicionSiguienteRutaActual;
			//Variables de patrulla para cuando el enemigo ha salido de la ruta de patrulla tras perseguir al jugador
		private List<Punto> rutaAlPuntoMasCercano;
		private int indiceRutaAlMasCercano;
		private bool fueraDeRuta = false;
		private int puntoMasCercano;

        //Variables de seguimiento
        public int velocidadSeguimiento;

        public Punto Posicion
        {
            get
            {
                return transform.position;
            }
        }


        private void Start()
        {
            estado = Estado.Patrullando;
            vivo = true;

			rutas = new List<Punto>[ puntosDeSeguimiento.Length];

			for (int i = 0; i < puntosDeSeguimiento.Length; i++) {
				int siguientePunto = (i + 1) % puntosDeSeguimiento.Length;
				Debug.Log ("I = " + i+ "  , rutasLength = "+rutas.Length);
				Debug.Log (puntosDeSeguimiento [i]);
				Debug.LogWarning (puntosDeSeguimiento [siguientePunto]);
				rutas [i] = 
					Buscador.Buscar (
					GameManager.Mapa, 
					puntosDeSeguimiento [i], 
					puntosDeSeguimiento [siguientePunto])
				;
				Debug.Log ("I2 = " + i+ "  , rutasLength = "+rutas.Length);

			}


            StartCoroutine(ControlIA());
        }

        IEnumerator ControlIA()
        {
			float velocidadActual;
            while ( vivo )
            {
				switch (estado) 
				{
				case Estado.Patrullando:
					Patrullar ();
					velocidadActual = 1f/velocidadPatrulla;
					break;
				case Estado.Persiguiendo:
					Perseguir ();
					velocidadActual = 1f / velocidadSeguimiento;
					break;
				default:
					velocidadActual = 1;
					break;
				}
				yield return new WaitForSeconds ( velocidadActual ) ;
            }
        }

		private void Patrullar()
		{
			if (fueraDeRuta) {
				indiceRutaAlMasCercano++;
				transform.position = rutaAlPuntoMasCercano [indiceRutaAlMasCercano];

				//Si he llegado al punto más cercano , ya no estoy fuera de ruta
				if (Posicion.Equals (puntosDeSeguimiento [puntoMasCercano])) {
					fueraDeRuta = false;
				}

			} else {
				transform.position = rutas [puntoActual] [posicionSiguienteRutaActual];

				if (Posicion.Equals ((Punto)puntosDeSeguimiento [(puntoActual + 1) % puntosDeSeguimiento.Length])) {
					puntoActual = (puntoActual + 1) % puntosDeSeguimiento.Length;
					posicionSiguienteRutaActual = 1;
				} else {
					posicionSiguienteRutaActual++;
				}

			}

			if (Punto.Distancia (Posicion, GameManager.Jugador.Posicion) < 4) {
				estado = Estado.Persiguiendo;
			}
		}

		private void Perseguir ()
		{
			if ( Posicion.Equals ( GameManager.Jugador.Posicion ) )
				return;

			List<Punto> rutaAlJugador = CalcularRuta (GameManager.Jugador.Posicion);
			transform.position = rutaAlJugador [1];

			if (Punto.Distancia (Posicion, GameManager.Jugador.Posicion) >= 4) {
				estado = Estado.Patrullando;
				fueraDeRuta = true;
				indiceRutaAlMasCercano = 0;

				List<Punto> aux;
				rutaAlPuntoMasCercano = new List<Punto> ();

				for (int i = 0; i < puntosDeSeguimiento.Length; i++) {
					aux = CalcularRuta (puntosDeSeguimiento [i]);
					if (i==0 || aux.Count < rutaAlPuntoMasCercano.Count) {
						rutaAlPuntoMasCercano = aux;
					}
				}



				rutaAlPuntoMasCercano = null;

				//TODO Calcular la ruta hacia el punto más cercano
				if (rutaAlPuntoMasCercano == null) {
					rutaAlPuntoMasCercano = CalcularRuta (GameManager.Jugador.Posicion);
				}

			}
		}

        public void Mover (){
			//Punto posicion = CalcularRuta()[1];
			//transform.position = posicion;
			//EventManager.MoverEnemigo (transform.position);
		}

        List<Punto> CalcularRuta ( Punto puntoObjetivo )
        {
            
            return Buscador.Buscar(GameManager.Mapa, Posicion, puntoObjetivo);
        }
    }
}
