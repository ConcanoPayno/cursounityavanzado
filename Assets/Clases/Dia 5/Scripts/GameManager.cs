﻿using UnityEngine;
using CursoAvanzado.Dia4;
using System.Collections.Generic;

namespace CursoAvanzado.Dia5
{
    public class GameManager : MonoBehaviour
    {
		public GameObject enemigoPrefab;

        #region  Singleton
        private static GameManager _singleton;
        
        public static GameManager Instancia
        {
            get
            {
                if (_singleton == null)
                {
                    GameObject go = GameObject.Find("GameManager");

                    if (go != null)
                    {
                        _singleton = go.GetComponent<GameManager>();

                        if (_singleton == null)
                        {
                            _singleton = go.AddComponent<GameManager>();
                        }
                    }
                    else
                    {
                        go = new GameObject("GameManager");
                        _singleton = go.AddComponent<GameManager>();
                    }
                }

                return _singleton;
            }

            set
            {
				Debug.Log ("Nuevo GameManager = " + value);
				if (_singleton != null) {
					Debug.Log ("Antiguo GameManager = " + _singleton);
					Destroy (_singleton.gameObject);
				}
                _singleton = value;
            }

        }
        #endregion

        #region Variables_Y_Propiedades
        private Mapa mapa;
        public static Mapa Mapa
        {
            get
            {
                if ( Instancia.mapa == null )
                {
                    Instancia.mapa = FindObjectOfType<Mapa>();
                }
                return Instancia.mapa;
            }
        }


        private Jugador jugador;
        public static Jugador Jugador
        {
            get
            {
                if (Instancia.jugador == null)
                {
                    Instancia.jugador = FindObjectOfType<Jugador>();
                }
                return Instancia.jugador;
            }
        }

        private List<Enemigo> enemigos;
        public static List<Enemigo> Enemigos
        {
            get
            {
                if (Instancia.enemigos == null)
                {
                    Instancia.enemigos = new List<Enemigo>(FindObjectsOfType<Enemigo>());
                }
                return Instancia.enemigos;
            }
        }
        #endregion

        #region MonoBehaviour_Metodos
        private void Awake()
        {
            Instancia = this;
			mapa = GameManager.Mapa;

			enemigos = new List<Enemigo> ();

			GameObject enemigo1 = Instantiate (enemigoPrefab, mapa.transform);
			enemigo1.transform.localPosition = new Vector2 (1, mapa.alto - 2);
			enemigos.Add (enemigo1.GetComponent<Enemigo> ());

			GameObject enemigo2 = Instantiate (enemigoPrefab, mapa.transform);
			enemigo2.transform.localPosition = new Vector2 (mapa.ancho - 2, 1);
			enemigos.Add (enemigo2.GetComponent<Enemigo> ());

			EventManager.Instancia.OnThunder += SeHaLanzadoUnTrueno;
        }
        #endregion

        #region GameManager_Metodos

		public void JugadorSeMueve ()
		{
			Debug.Log ("Jugador se mueve ");
			foreach (var enemigo in enemigos) {
				enemigo.Mover ();
			}

			LanzarTrueno ();
		}

		public void LanzarTrueno(){
			Debug.Log ("Lanzamos el trueno");
			EventManager.MakeThunderStatic ();
		}

		private void SeHaLanzadoUnTrueno (){
			Debug.Log ("Se ha lanzado un trueno");
		}

        #endregion

    }
}
