﻿using System.Collections.Generic;
using UnityEngine;
using CursoAvanzado.Dia4;

namespace CursoAvanzado.Dia5
{
    public class Enemigo : MonoBehaviour
    {
        Mapa mapa;
        Jugador jugador;

        public Punto Posicion
        {
            get
            {
                return new Punto(Mathf.FloorToInt(transform.position.x), Mathf.FloorToInt(transform.position.y));
            }
        }
        private void Awake()
        {
            mapa = GameManager.Mapa;
            jugador = GameManager.Jugador;
            
        }
        
		public void Mover (){
			Punto posicion = CalcularRuta()[1];
			transform.position = posicion;
			EventManager.MoverEnemigo (transform.position);
		}

        List<Punto> CalcularRuta ( )
        {
            
            return Buscador.Buscar(mapa, Posicion, jugador.Posicion);
        }
    }
}
