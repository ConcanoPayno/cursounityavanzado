﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CursoAvanzado.Dia4;

namespace CursoAvanzado.Dia5
{
    public class Jugador : MonoBehaviour
    {
		static Jugador jugador;
        public int vidas = 3;
        public int rebobinados = 2; //Número de veces que podrá rebobinar la jugada

        public Punto Posicion
        {
            get
            {
				return transform.position;
            }
        }

		void Start (){
			jugador = this;
			EventManager.Instancia.OnMovimientoEnemigo += ComprobarDistancia;
		}

		private void ComprobarDistancia (Punto punto){
			if (Vector3.Distance (punto, Posicion) < 3) {
				Debug.LogError (" CUIDADOO!!!!!");
			}
		}

		public  void CambiarNombreJugador ( string nuevoNombre ){
			name = nuevoNombre;
		}

		public void DevolverValor ( Punto valor ){
			Debug.Log ("Valor del slider = " + valor);
		}
    }
}
