﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CursoAvanzado.Dia4;

namespace CursoAvanzado.Dia5
{
	public class EventManager {

		#region  Singleton
		private static EventManager _singleton;

		public static EventManager Instancia
		{
			get
			{
				if (_singleton == null)
				{
					_singleton = new EventManager ();
				}

				return _singleton;
			}
		}
		#endregion

		public event Action OnThunder;

		public void MakeThunder (){
			if (OnThunder != null)
				OnThunder();
		}

		public static void MakeThunderStatic (){
			Instancia.MakeThunder ();
		}

		public event Action<Punto> OnMovimientoEnemigo;

		public static void MoverEnemigo (Punto punto ){
			if (Instancia.OnMovimientoEnemigo != null) {
				Instancia.OnMovimientoEnemigo (punto);
			}
		}

		public event EventHandler<MiEventoArg> evento;

		public static void OtroEvento ( object sender, MiEventoArg argumento )
		{
			if (Instancia.evento != null)
				Instancia.evento (sender, argumento);
		}

	}

	public class MiEventoArg : EventArgs{
		public float TiempoDeLanzamiento;
		public Punto Punto;
		public GameObject objetivo;
	}
}
