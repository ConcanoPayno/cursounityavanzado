﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OtraClase : MonoBehaviour {
	Camera m_Camera;
	public GameObject prefabBala;

	void Start(){
		Debug.Log (GameManager.Buhos ());
		m_Camera = Camera.main;
	}

	void Update (){
		if (Input.GetMouseButtonDown (0)) {
			if (GameManager.Balas () > 0) {
				GameManager.CapturarBuho ();
				AudioManager.SonidoDisparo ();
				Debug.Log (GameManager.Buhos ());
				Debug.Log ("Balas->" + GameManager.Balas ());


				Vector3 mousePosition = Input.mousePosition;
				mousePosition.z = 500;
				mousePosition = m_Camera.ScreenToWorldPoint (mousePosition);
				Quaternion direccion = Quaternion.FromToRotation(transform.position, mousePosition);
                
                Instantiate (prefabBala, transform.position, direccion).transform.LookAt(mousePosition);
                
			} else {
				//Audio disparo sin balas en recámara
			}
		}

		if (Input.GetKeyDown (KeyCode.R)) {
			GameManager.Recargar ();
			AudioManager.Recarga ();
		}
	}
}
