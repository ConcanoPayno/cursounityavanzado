﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource) )]
public class AudioManager : MonoBehaviour {

	public AudioClip disparo, recarga;

	private AudioSource audioSource;

	private static AudioManager _singleton;

	public static AudioManager Instancia {
		get {
			if (_singleton == null) {
				GameObject go = GameObject.Find ("AudioManager");

				if (go != null) {
					_singleton = go.GetComponent<AudioManager> ();

					if (_singleton == null) {
						_singleton = go.AddComponent<AudioManager> ();
					}
				} else {
					go = new GameObject ("AudioManager");
					_singleton = go.AddComponent<AudioManager> ();
				}
			}

			return _singleton;
		}
	}

	void Awake(){
		audioSource = GetComponent<AudioSource> ();
	}

	public static void SonidoDisparo(){
		Instancia.audioSource.PlayOneShot ( Instancia.disparo );

	}

	public static void Recarga(){
		Instancia.audioSource.PlayOneShot ( Instancia.recarga );

	}
}
