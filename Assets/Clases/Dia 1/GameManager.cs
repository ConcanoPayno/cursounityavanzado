﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {


	private static GameManager _singleton;

	public Text txtBalas, txtAciertos;

	public static GameManager Instancia {
		get
		{
			if (_singleton == null) {
				GameObject go = GameObject.Find ("GameManager");

				if (go != null) {
					_singleton = go.GetComponent<GameManager> ();

					if (_singleton == null) {
						_singleton = go.AddComponent<GameManager> ();
					}
				} else {
					go = new GameObject ("GameManager");
					_singleton = go.AddComponent<GameManager> ();
				}
			}

			return _singleton;
		}

		set
		{
			if ( _singleton != null )
				Destroy (_singleton.gameObject);
			_singleton = value;
		}

	}

	private int buhos= 20;
	private int balas = 3; //Balas en la recámara
	private int disparos = 0; // Numero de disparos realizados
	private int aciertos = 0; 

	void Awake (){
		GameManager.Instancia = this;
		Debug.Log ("Awake el singleton es "+_singleton.name);
	}

	void Start (){
		Debug.Log ("Start");
	}
	public static int Buhos (){
		return Instancia.buhos;
	}

	public static int Balas(){
		return Instancia.balas;
	}

	public static void CapturarBuho (){
		Instancia.buhos = Instancia.buhos - 1;
		Instancia.balas -= 1;
		Instancia.disparos += 1;
		Instancia.txtBalas.text = ""+ Instancia.balas;
	}

	public static void Recargar(){
		Instancia.balas = 3;
		Instancia.txtBalas.text = ""+ Instancia.balas;
	}

	public static void Acierto (){
		Instancia.aciertos++;
		Instancia.txtAciertos.text = "" + Instancia.aciertos;
	}
}
