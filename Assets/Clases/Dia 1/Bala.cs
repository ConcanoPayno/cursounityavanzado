﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody) )]
public class Bala : MonoBehaviour {

	Rigidbody body;
	public float velocidad = 5;

	// Use this for initialization
	void Start () {
		body = GetComponent<Rigidbody> ();
        body.AddRelativeForce ( Vector3.forward * velocidad, ForceMode.Force);
		Destroy (gameObject, 10);
	}


	void OnTriggerEnter ( Collider col ){
        if ( col.CompareTag ( "BlueBird" ) ){
			GameManager.Acierto();
		}
	}
}
