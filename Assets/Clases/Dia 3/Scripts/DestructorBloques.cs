﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructorBloques : MonoBehaviour {

	void OnEnable ()
	{
		Invoke ("DestruirBloque", 10f);
	}

	void DestruirBloque ()
	{
		gameObject.SetActive (false);
	}

	void OnDisable ()
	{
		CancelInvoke ();
	}
}
