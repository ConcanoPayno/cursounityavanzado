﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent ( typeof ( Collider2D ) )]
public class BalaRunner : MonoBehaviour {

	void OnCollisionEnter2D (Collision2D col )
	{
		if (col.collider.CompareTag ("Bloque")) {
			
			col.collider.gameObject.SetActive (false);
			gameObject.SetActive (false);
		}
	}
}
