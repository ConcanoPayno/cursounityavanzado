﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour {

	public int maxObjetos = 5;

	public List<GameObject> prefabsObjetos;
	List<GameObject> objetos;



	// Use this for initialization
	void Start () {
		objetos = new List<GameObject> ();

		for (int i = 0; i < maxObjetos ; i++) {
			for (int j = 0; j < prefabsObjetos.Count; j++) {
				GameObject obj = (GameObject)Instantiate (prefabsObjetos[j], transform);
				obj.SetActive (false);
				objetos.Add (obj);
			}
		}

		StartCoroutine(GeneraBloque());
	}

	IEnumerator GeneraBloque()
	{
		while ( true)
		{
			yield return new WaitForSeconds (3);

			GameObject obj = GetBloque ();
			obj.transform.localPosition = new Vector3 (14, 1.5f * Random.Range (-1, 2), 0);
			obj.SetActive (true);
		}
	}

	GameObject GetBloque (){

		List<int> indices = new List<int> ();

		for (int i = 0; i < objetos.Count; i++) {
			if (!objetos [i].activeInHierarchy)
				indices.Add (i);
		}

		return objetos [indices [Random.Range (0, indices.Count)]];
	}
}
