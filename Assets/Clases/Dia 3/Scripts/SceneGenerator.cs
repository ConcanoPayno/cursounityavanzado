﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class SceneGenerator : MonoBehaviour {

	public GameObject prefabPieza;
	public int maxPiezas = 3;

	public int maxObjetos = 5;

	public List<GameObject> prefabsObjetos;

	private List<GameObject> objetos;
	private List<GameObject> piezas;
	private float xPieza = 0;
	private BoxCollider2D box;

	void Awake (){
		box = GetComponent<BoxCollider2D> ();

		piezas = new List<GameObject> ();
		for (int i = 0; i < maxPiezas; i++) {
			GameObject pieza = (GameObject)Instantiate (prefabPieza, transform);
			pieza.SetActive (false);
			piezas.Add (pieza);
		}

		objetos = new List<GameObject> ();

		for (int i = 0; i < maxObjetos ; i++) {
			for (int j = 0; j < prefabsObjetos.Count; j++) {
				GameObject obj = (GameObject)Instantiate (prefabsObjetos[j], transform);
				obj.SetActive (false);
				objetos.Add (obj);
			}
		}

		RecolocaPieza ();

	}

	GameObject GetBloque (){

		List<int> indices = new List<int> ();

		for (int i = 0; i < objetos.Count; i++) {
			if (!objetos [i].activeInHierarchy)
				indices.Add (i);
		}

		return objetos [indices [Random.Range (0, indices.Count)]];
	}

	void OnTriggerEnter2D ( Collider2D col )
	{
		if (col.CompareTag ("Player")) {
			RecolocaPieza ();
		}
	}

	void RecolocaPieza ()
	{

		box.offset = Vector2.right * xPieza;

		int seleccion = -1;
		int libres = 0;

		for (int i = 0; i < maxPiezas; i++) {
			if (!piezas [i].activeInHierarchy) {
				if (seleccion == -1) {
					piezas [i].transform.localPosition = Vector2.right * xPieza; //new Vector3 ( xPieza, 0, 0);
					piezas [i].SetActive (true);
					float xOffset = -10;
					for (int j = 0; j < 3; j++) {
						GameObject bloque = GetBloque ();
						if (bloque != null) {
							bloque.transform.parent = piezas [i].transform;
							bloque.transform.localPosition = new Vector2 (xOffset, 1.5f * Random.Range (-1, 2));
							bloque.SetActive (true);
						}
						xOffset += 10;
					}
					seleccion = i;
				} else {
					libres++;
				}
			}
		}

		if (libres == 0) {
			int index = (seleccion + 1) % maxPiezas;
			piezas [index].SetActive (false);
			foreach (Transform t in piezas[index].transform) {
				if ( t.CompareTag("Bloque" ) )
					t.gameObject.SetActive (false);
			}
		}


		xPieza += 30;

	}
}
