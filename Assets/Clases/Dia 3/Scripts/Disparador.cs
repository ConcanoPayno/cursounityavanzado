﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparador : MonoBehaviour {

	List<GameObject> balas;
	public GameObject prefabBala;
	public int recamara;



	void Start ()
	{
		balas = new List<GameObject> ();

		for (int i = 0; i < recamara; i++) {
			GameObject bala = (GameObject)Instantiate (prefabBala, transform);
			bala.SetActive (false);
			balas.Add (bala);
		}
	}

	public void Disparar (){
		for (int i = 0; i < recamara; i++) {
			if (!balas [i].activeInHierarchy) {
				balas [i].transform.localPosition = Vector3.zero;
				balas [i].SetActive (true);
				balas [i].GetComponent<Rigidbody2D> ().AddForce (Vector2.right * 500);

				break;
			}
		}
	}
}
