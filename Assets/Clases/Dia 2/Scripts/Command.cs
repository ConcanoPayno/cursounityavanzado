﻿using UnityEngine;

public abstract class Command {

    //Tiempo transcurrido desde el último commando
    protected float _time;

    public float time{ get { return _time; } }

    public abstract void Execute(Rigidbody2D body);

    //
    public virtual void Undo(Rigidbody2D body) { }

    //
    public virtual void Do(Rigidbody2D body) { }
}


public class SwapGravity : Command
{
    public override void Execute(Rigidbody2D body)
    {
        _time = Time.time;
        Do(body);

        InputHandler.oldCommands.Add(this);
    }

    //
    public override void Undo(Rigidbody2D body)
    {
        body.gravityScale *= -1;
    }

    //
    public override void Do(Rigidbody2D body)
    {
        body.gravityScale *= -1;
    }
}

public class Scale : Command
{
    public override void Execute(Rigidbody2D body)
    {
		_time = Time.time;
		Do(body);

		InputHandler.oldCommands.Add(this);
    }

    //
    public override void Undo(Rigidbody2D body)
    {
		body.transform.localScale *= 0.5f;
    }

    //
    public override void Do(Rigidbody2D body)
    {
		body.transform.localScale *= 2;
    }
}

public class Rewind : Command
{
    public override void Execute(Rigidbody2D body)
    {
        InputHandler.rewind = true;
    }
}