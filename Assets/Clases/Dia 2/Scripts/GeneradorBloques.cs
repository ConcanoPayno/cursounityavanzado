﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneradorBloques : MonoBehaviour {

    public GameObject[] prefabsBloque;

    private List<GameObject> bloques = new List<GameObject>();

    public static float speed = 3;

	private float startRewindTime = -1;
	private bool endRewindTime = true;
	private float lastBlockTime = -1;
	private float timeBetweenBlocks = 3;
	// Use this for initialization
	void Start () {
        StartCoroutine(GeneraBloque());
	}

    private void Update()
    {
        foreach ( var b in bloques)
        {
            b.transform.Translate(Vector3.left * Time.deltaTime * speed);
        }
    }

    IEnumerator GeneraBloque()
    {
        while ( true)
        {
			if (speed > 0) {
				
				if (!endRewindTime) {
					yield return new WaitForSeconds (InputHandler.rewindTime+timeBetweenBlocks - (startRewindTime - lastBlockTime));
				}
				endRewindTime = true;
				startRewindTime = -1;

				GameObject bloque = Instantiate (prefabsBloque [Random.Range (0, prefabsBloque.Length)], transform);

				bloque.transform.localPosition = new Vector3 (14, 1.5f * Random.Range (-1, 2), 0);
				bloque.SetActive (true);
				lastBlockTime = Time.time;
				bloques.Add (bloque);

			} else if ( startRewindTime == -1 ){
				startRewindTime = Time.time;
				endRewindTime = false;

				Debug.Log ("Tiempo: " + Time.time + " StartRewind: " + startRewindTime + "  , LastBlock: " + lastBlockTime);
				yield return new WaitForSeconds (InputHandler.rewindTime);
			}

			yield return new WaitForSeconds(timeBetweenBlocks); //Segundos que esperamos entre cada iteración

        }
    }
	
}
