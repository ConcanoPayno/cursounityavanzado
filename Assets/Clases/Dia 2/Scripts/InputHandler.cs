﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D), typeof(Disparador))]
public class InputHandler : MonoBehaviour {

    public static bool rewind = false;
    
    public static List<Command> oldCommands = new List<Command>();

	public static float rewindTime = 5;

    private Rigidbody2D body;
	private Disparador disparador;
    private bool isReplaying;
    private Coroutine replayCoroutine;

    private void Start()
    {
        body = GetComponent<Rigidbody2D>();
		disparador = GetComponent<Disparador> ();
    }
    private void Update()
    {
        if (!isReplaying)
            HandleInput();
        if (rewind)
            Rewind();

		transform.Translate(Vector3.right * Time.deltaTime * (isReplaying ? -3 : 3));
    }

    private void HandleInput()
    {
		if (Input.GetKeyDown (KeyCode.Space)) {
			new SwapGravity ().Execute (body);
		} else if (Input.GetKeyDown (KeyCode.R)) {
			new Rewind ().Execute (body);
		} else if (Input.GetKeyDown (KeyCode.S)) {
			new Scale ().Execute (body);
		} else if (Input.GetKeyDown (KeyCode.LeftControl)) {
			disparador.Disparar ();
		}
    }

    private void Rewind()
    {
        //if ( oldCommands.Count > 0)
        {
            rewind = false;

            if ( replayCoroutine != null)
            {
                StopCoroutine(replayCoroutine);
            }

            replayCoroutine = StartCoroutine(ReplayCommands());
        }
    }

    IEnumerator ReplayCommands()
    {
        isReplaying = true;
        
        float waitTime = Time.time;
        float startTime = Time.time;
        GeneradorBloques.speed *= -1;
		int j = 0;

		//Rebobinamos las acciones realizadas
		if (oldCommands.Count > 0) {
			for (int i = oldCommands.Count - 1; i >= 0; i--) {
				j++;
				waitTime -= oldCommands [i].time;
				yield return new WaitForSeconds (waitTime);
				waitTime = oldCommands [i].time;

				oldCommands [i].Undo (body);
			}
		}

		oldCommands = new List<Command> ();

		Debug.Log (Time.time+" Speed : "+GeneradorBloques.speed+ "  StartTime: "+startTime );
        //Rebobinamos en total 5 segundos contando lo anterior
		if (startTime + rewindTime > Time.time) {
			yield return new WaitForSeconds (rewindTime - (Time.time - startTime)); 
		}
		GeneradorBloques.speed *= -1;

        isReplaying = false;
    }

	void OnCollisionEnter2D (Collision2D col){
		if ( col.collider.CompareTag("Bloque" ) && !isReplaying)
			new Rewind().Execute(body);
	}
}
