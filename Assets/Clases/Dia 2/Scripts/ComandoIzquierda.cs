﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComandoIzquierda : Comando {

	public override void Ejecutar(){

		Hacer ();

		ControlEntrada.oldCommands.Add (this);
	}

	public override void Hacer(){
		this.objeto.Translate (Vector3.left);
	}

	public override void Deshacer(){
		this.objeto.Translate (Vector3.right);
	}
}
