﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Comando {
	public Transform objeto;

	public abstract void Ejecutar ();

	public virtual void Hacer (){
	}

	public virtual void Deshacer(){
	}
}
	