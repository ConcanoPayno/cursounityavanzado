﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComandoAbajo : Comando {

	public override void Ejecutar(){

		Hacer ();

		ControlEntrada.oldCommands.Add (this);
	}

	public override void Hacer(){
		this.objeto.Translate (Vector3.down);
	}

	public override void Deshacer(){
		this.objeto.Translate (Vector3.up);
	}
}
