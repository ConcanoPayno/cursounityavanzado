﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace UnityAvanzado.Patrones
{
    public class InputHandler : MonoBehaviour
    {
        //The different keys we need
        private Command buttonW, buttonS, buttonA, buttonD, buttonB, buttonZ, buttonR;
        //Stores all commands for replay and undo
        public static List<Command> oldCommands = new List<Command>();
        //Box start position to know where replay begins
        private Vector3 boxStartPos;
        //To reset the coroutine
        private Coroutine replayCoroutine;
        //If we should start the replay
        public static bool shouldStartReplay;
        //If we should start the rewind
        public static bool shouldStartRewind;
        //So we cant press keys while replaying
        private bool isReplaying;


        void Start()
        {
            //Bind keys with commands
            buttonB = new DoNothing();
            buttonZ = new UndoCommand();
            buttonR = new ReplayCommand();

            buttonA = new MoveLeft();
            buttonD = new MoveRight();
            boxStartPos = transform.position;
        }



        void Update()
        {
            if (!isReplaying)
            {
                HandleInput();
            }

            StartReplay();
        }


        //Check if we press a key, if so do what the key is binded to 
        public void HandleInput()
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                buttonA.Execute(transform, buttonA);
            }
            else if (Input.GetKeyDown(KeyCode.B))
            {
                buttonB.Execute(transform, buttonB);
            }
            else if (Input.GetKeyDown(KeyCode.D))
            {
                buttonD.Execute(transform, buttonD);
            }
            else if (Input.GetKeyDown(KeyCode.R))
            {
                buttonR.Execute(transform, buttonZ);
            }
            else if (Input.GetKeyDown(KeyCode.S))
            {
                buttonS.Execute(transform, buttonS);
            }
            else if (Input.GetKeyDown(KeyCode.W))
            {
                buttonW.Execute(transform, buttonW);
            }
            else if (Input.GetKeyDown(KeyCode.Z))
            {
                buttonZ.Execute(transform, buttonZ);
            }
        }


        //Checks if we should start the replay
        void StartReplay()
        {
            if (shouldStartReplay && oldCommands.Count > 0)
            {
                shouldStartReplay = false;

                //Stop the coroutine so it starts from the beginning
                if (replayCoroutine != null)
                {
                    StopCoroutine(replayCoroutine);
                }

                //Start the replay
                replayCoroutine = StartCoroutine(ReplayCommands(transform));
            }
        }

        //Checks if we should start the replay
        void StartRewind()
        {
            if (shouldStartRewind && oldCommands.Count > 0)
            {
                shouldStartRewind = false;
                
                //Stop the coroutine so it starts from the beginning
                if (replayCoroutine != null)
                {
                    StopCoroutine(replayCoroutine);
                }

                //Start the replay
                replayCoroutine = StartCoroutine(RewindCommands(transform));
            }
        }

        //The replay coroutine
        IEnumerator RewindCommands(Transform transform)
        {
            //So we can't move the box with keys while replaying
            isReplaying = true;
            
            for (int i = oldCommands.Count-1; i >= 0; i--)
            {
                //Move the box with the current command
                oldCommands[i].Undo(transform);

                yield return new WaitForSeconds(0.3f);
            }

            //We can move the box again
            isReplaying = false;
        }
        //The replay coroutine
        IEnumerator ReplayCommands(Transform transform)
        {
            //So we can't move the box with keys while replaying
            isReplaying = true;

            //Move the box to the start position
            transform.position = boxStartPos;

            for (int i = 0; i < oldCommands.Count; i++)
            {
                //Move the box with the current command
                oldCommands[i].Do(transform);

                yield return new WaitForSeconds(0.3f);
            }

            //We can move the box again
            isReplaying = false;
        }
    }
}