﻿using UnityEngine;

namespace UnityAvanzado.Patrones {
    public class GameController : MonoBehaviour
    {
        public int predatorsAlive;
        public int preysAlive;

        private void Awake()
        {
            Singleton<GameController>.Instance = this;
        }
        
    }
}