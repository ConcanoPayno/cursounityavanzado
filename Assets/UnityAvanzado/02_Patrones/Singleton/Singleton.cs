﻿using UnityEngine;

namespace UnityAvanzado.Patrones
{
    public class Singleton<T> where T : MonoBehaviour 
    {
        protected static T m_Singleton;

        /// <summary>
        /// Singleton value for type T
        /// </summary>
        public static T Instance
        {
            get
            {
                //Si aún no se ha asignado el valor al singleton
                if (m_Singleton == null)
                {
                    //Buscamos un posible objeto en la escena que sea del tipo del singleton
                    T someObject = Object.FindObjectOfType<T>();

                    //Si existe dicho objeto, lo asignamos
                    if (someObject != null)
                    {
                        m_Singleton =  someObject;
                    }
                    else
                    {
                        //Si no existe lo creamos y asignamos como singleton
                        GameObject anotherObject = new GameObject(typeof(T).Name+" Singleton", typeof(T));
                        m_Singleton = anotherObject.GetComponent<T>();
                    }
                }
                return m_Singleton;
            }
            set
            {
                //Si la variable singleton no se ha inicializado asignamos el valor correspondiente
                if (m_Singleton == null)
                    m_Singleton = value;
                //Si la variable singleton ya tiene un valor asignado asignamos el actual
                else
                {
                    m_Singleton = value;
                }
            }
        }
    }
}