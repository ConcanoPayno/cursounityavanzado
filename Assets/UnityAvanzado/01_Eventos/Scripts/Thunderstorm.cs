﻿using UnityEngine;
using System.Collections;

namespace UnityAvanzado.Eventos
{
    [RequireComponent(typeof(AudioSource),typeof(Light))]
    public class Thunderstorm : MonoBehaviour
    {
        public float m_MinRate, m_MaxRate;          //Mínimo y máximo número de truenos por segundo

        [SerializeField]
        private AudioClip m_ThunderClip;            //Audio de trueno

        private AudioSource m_AudioSource;          //Fuente de sonido
        private Light m_Light;                      //Luz que simula los relámpagos
        private float m_NextThunderTime;            //Tiempo que transcurrirá hasta el siguiente trueno

        /// <summary>
        /// Asigna las variables de Luz y Sonido, e incia el lanzamiento del primero relámpago+trueno.
        /// </summary>
        void Start()
        {
            m_Light = GetComponent<Light>();
            m_Light.intensity = 0;

            m_AudioSource = GetComponent<AudioSource>();
            
            StartCoroutine(Thunder());
        }
        /// <summary>
        /// Lanza un relámpago y seguido un trueno.
        /// </summary>
        IEnumerator Thunder()
        {
            //Esperamos a lanzar el primer trueno
            yield return new WaitForSeconds(1 / (Random.Range(m_MinRate, m_MaxRate)));

            //Antes del trueno los relámpagos han de iluminar todo
            m_Light.intensity = 2;
            yield return new WaitForSeconds(0.1f);
            m_Light.intensity = 1;
            yield return new WaitForSeconds(0.1f);
            m_Light.intensity = 2;
            yield return new WaitForSeconds(0.1f);
            m_Light.intensity = 0;

            //Suena el trueno
            m_AudioSource.PlayOneShot(m_ThunderClip);
            
            //Avisamos a quien escuche que el trueno ha sonado
            EventManager.Thunder();

            //Cargamos otro trueno de nuevo
            yield return Thunder();
        }
    }
}