﻿using System;

namespace UnityAvanzado.Eventos
{
    public class EventManager
    {
        public static event Action OnThunder;
        public static void Thunder()
        {
            if ( OnThunder != null)
            {
                OnThunder();
            }
        }
        
    }
}
