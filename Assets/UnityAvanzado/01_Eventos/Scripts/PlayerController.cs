﻿using UnityEngine;
using UnityAvanzado.Patrones;

namespace UnityAvanzado.Eventos
{
    public class PlayerController : MonoBehaviour
    {
        private Camera m_Camera;

        // Use this for initialization
        void Start()
        {
            m_Camera = Camera.main;
            
            Singleton <PlayerController>.Instance = this;
        }

        // Update is called once per frame
        void Update()
        {
            Vector3 mousePosition = Input.mousePosition;
            mousePosition.z = 50;
            mousePosition = m_Camera.ScreenToWorldPoint (mousePosition);
            
            transform.LookAt ( mousePosition );
        }
    }
}