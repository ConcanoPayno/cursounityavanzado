﻿using UnityEngine;
using System.Collections;

namespace UnityAvanzado.Eventos
{
    public class Sleeper : MonoBehaviour
    {
        public float m_Speed = 2;
        public Vector2 m_HorizontalMargins = new Vector2(-12,12);
        public float m_PauseTime = 1;
        public float m_Sprint = 4;

        private Vector3 m_NormalPosition;
        private Vector2 m_PerlinNoise;

        private bool m_Moving = true;
        private bool m_Shaking = false;
        private int m_Direction;
        private float m_CurrentSprint = 0;

        private void OnEnable()
        {
            EventManager.OnThunder += AwakeSleeper;
            m_Direction = 1 - 2 * Random.Range(0, 2);
            m_NormalPosition = transform.position;
        }

        private void OnDisable()
        {
            EventManager.OnThunder -= AwakeSleeper;
        }

        private void Update()
        {
            if (m_Moving)
            {
                m_NormalPosition += Vector3.right * Time.deltaTime * m_Direction * ( m_Speed + m_CurrentSprint);
                m_CurrentSprint -= Time.deltaTime;
                if (m_CurrentSprint < 0)
                    m_CurrentSprint = 0;

                if ( m_NormalPosition.x > m_HorizontalMargins.y ) 
                {
                    m_NormalPosition.x = m_HorizontalMargins.y;
                    m_Direction *= -1;
                    StartCoroutine(Wait());
                }
                else if (m_NormalPosition.x < m_HorizontalMargins.x)
                {
                    m_NormalPosition.x = m_HorizontalMargins.x;
                    m_Direction *= -1;
                    StartCoroutine(Wait());
                }
            }
            if (!m_Shaking)
                transform.position = m_NormalPosition;
        }

        private void AwakeSleeper()
        {
            m_NormalPosition = transform.position;
            StartCoroutine(Shake());
        }

        private IEnumerator Wait()
        {
            m_Moving = false;
            yield return new WaitForSeconds(m_PauseTime);

            m_CurrentSprint = m_Sprint;
            if ( !m_Shaking )
                m_Moving = true;
        }

        private IEnumerator Shake()
        {
            m_Shaking = true;
            m_Moving = false;
            yield return new WaitForSeconds(0.6f);
            float startTime = Time.time;
            float noise;
            while (Time.time - startTime < 3)
            {
                m_PerlinNoise += new Vector2 ( 0.95f, 0f);
                noise = Mathf.PerlinNoise(m_PerlinNoise.x, m_PerlinNoise.y) * Random.Range(-1, 2) * 0.05f;
                yield return transform.position = m_NormalPosition + noise * Vector3.right;
            }
            transform.position = m_NormalPosition;
            m_Shaking = false;

            m_Direction = 1 - 2 * Random.Range(0, 2);

            yield return Wait();
        }

    }
}
